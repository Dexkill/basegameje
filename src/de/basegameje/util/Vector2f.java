package de.basegameje.util;

public class Vector2f {
	
	public float x, y;
	
	public Vector2f(){
		this.x = 0F;
		this.y = 0F;
	}
	
	public Vector2f(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	public Vector2f add(Vector2f other){
		return new Vector2f(x + other.x, y + other.y);
	}
	
	public static Vector2f add(Vector2f one, Vector2f other){
		return new Vector2f(one.x + other.x, one.y + other.y);
	}
	
	public Vector2f subtract(Vector2f other){
		return new Vector2f(x - other.x, y - other.y);
	}
	
	public static Vector2f subtract(Vector2f vector, Vector2f other){
		return new Vector2f(vector.x - other.x, vector.y - other.y);
	}
	
	public Vector2f multiply(float operator){
		return new Vector2f(x * operator, y * operator);
	}
	
	public static Vector2f multiply(Vector2f vector, float operator){
		return new Vector2f(vector.x * operator, vector.y * operator);
	}
	
	public static Vector2f normalize(Vector2f vector){
		return new Vector2f((float)Math.pow(vector.x, 0F), (float)Math.pow(vector.y, 0F));
	}
	
	public Vector2f normalize(){
		return new Vector2f((float)Math.pow(x, 0F), (float)Math.pow(y, 0F));
	}
	
	@Override
	public String toString(){
		return "Vector2f(" + x + ", " + y + ")";
	}
	
}
