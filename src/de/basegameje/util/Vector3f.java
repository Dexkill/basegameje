package de.basegameje.util;

public class Vector3f {

	public float x, y, z;
	
	public Vector3f(){
		this.x = 0F;
		this.y = 0F;
		this.z = 0F;
	}
	
	public Vector3f(float x, float y, float z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vector3f add(Vector3f other){
		return new Vector3f(x + other.x, y + other.y, z + other.z);
	}
	
	public static Vector3f add(Vector3f one, Vector3f other){
		return new Vector3f(one.x + other.x, one.y + other.y, one.z + other.z);
	}
	
	public Vector3f subtract(Vector3f other){
		return new Vector3f(x - other.x, y - other.y, z - other.z);
	}
	
	public static Vector3f subtract(Vector3f one, Vector3f other){
		return new Vector3f(one.x - other.x, one.y - other.y, one.z - other.z);
	}
	
	public Vector3f multiply(float operator){
		return new Vector3f(x * operator, y * operator, z * operator);
	}
	
	public static Vector3f multiply(Vector3f vector, float operator){
		return new Vector3f(vector.x * operator, vector.y * operator, vector.z * operator);
	}
	
	public static Vector3f normalize(Vector3f vector){
		return new Vector3f((float)Math.pow(vector.x, 0F), (float)Math.pow(vector.y, 0F), (float)Math.pow(vector.z, 0F));
	}
	
	public Vector3f normalize(){
		return new Vector3f((float)Math.pow(x, 0F), (float)Math.pow(y, 0F), (float)Math.pow(z, 0F));
	}
	
	@Override
	public String toString(){
		return "Vector2f(" + x + ", " + y + ", " + z + ")";
	}
}
