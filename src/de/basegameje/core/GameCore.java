package de.basegameje.core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import de.basegameje.managers.InputManager;
import de.basegameje.util.Time;

public abstract class GameCore implements Runnable{
	
	public abstract void onInit();
	public abstract void onDraw(Graphics2D g);
	public abstract void onUpdate();
	
	public static String TITLE = "Unknown";
	
	/* Screen resolution */
	public static int WIDTH;
	public static int HEIGHT;
	public static int WIN_WIDTH, WIN_HEIGHT;
	public static int SCALE;
	
	private Window gameWindow;
	private InputManager inputManager;
	
	private BufferedImage icon;
	
	private boolean running = false;
	
	public GameCore(String title, Dimension resolution, int scale, BufferedImage icon){
		TITLE = title;
		WIDTH = resolution.width;
		HEIGHT = resolution.height;
		SCALE =  scale;
		
		this.icon = icon;
	}
	
	public void start(){
		running = true;
		gameWindow = new Window(this.icon);
		inputManager = new InputManager(gameWindow);
		
		Thread game = new Thread(this);
		game.start();
	}
	
	@Override
	public void run() {
		long lastLoopTime = System.nanoTime();
	    final int TARGET_FPS = 60;
	    final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;
	    long lastFpsTime = 0;
	    
	    this.onInit();
	    
		while(running){
			long now = System.nanoTime();
	        long updateLength = now - lastLoopTime;
	        lastLoopTime = now;
	        Time.DELTATIME = updateLength / ((double)OPTIMAL_TIME);

	        lastFpsTime += updateLength;
	        if(lastFpsTime >= 1000000000){
	            lastFpsTime = 0;
	        }

	        this.update();

	        this.draw();

	        try{
	            Thread.sleep(10);
	        }catch(Exception e){
	        }
		}
		
		System.exit(0);
		
	}
	
	private void draw(){
		BufferStrategy bs = this.gameWindow.getBufferStrategy();
		
		if(bs == null){
			this.gameWindow.createBufferStrategy(3);
			return;
		}
		
		Graphics2D g = (Graphics2D)bs.getDrawGraphics();
		
		g.clearRect(0, 0, this.gameWindow.getWidth(), this.gameWindow.getHeight());
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, this.gameWindow.getWidth(), this.gameWindow.getHeight());
		
		/* draw */
		this.onDraw(g);
		
		g.dispose();
		bs.show();
	}
	
	private void update(){
		this.onUpdate();
	}
	
	public BufferedImage getImageFromRes(String path){
		try {
			return ImageIO.read(GameCore.class.getResourceAsStream(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Window getWindow(){
		return this.gameWindow;
	}
	
	public InputManager getInputManager(){
		return this.inputManager;
	}
	
	
}

