package de.basegameje.managers;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import de.basegameje.core.Window;
import de.basegameje.util.Vector2f;

public class InputManager implements KeyListener, MouseListener, MouseMotionListener{

	public InputManager(Window w){
		w.addKeyListener(this);
		w.addMouseMotionListener(this);
		mousePosition = new Vector2f();
	}
	
	public class Key{
		private boolean pressed = false, justPressed = false;
		
		public boolean isPressed(){
			return this.pressed;
		}
		
		public boolean isJustPressed(){
			boolean ret = this.justPressed;
			this.justPressed = false;
			return ret;
		}
	
		public void toogle(boolean isPressed){
			if(pressed == false && isPressed == true){
				this.justPressed = true;
			}
			
			this.pressed = isPressed;
		}
	}
	
	public List<Key> keys = new ArrayList<Key>();
	
	public Key key_w    = new Key();
	public Key key_s  = new Key();
	public Key key_a  = new Key();
	public Key key_d = new Key();
	public Key enter = new Key();
	public Key space = new Key();
	public Key esc   = new Key();
	
	public Vector2f mousePosition;

	@Override
	public void keyPressed(KeyEvent e) {
		toggleKey(e.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		toggleKey(e.getKeyCode(), false);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void toggleKey(int keyCode, boolean isPressed){
		
		if(keyCode == KeyEvent.VK_W){
			key_w.toogle(isPressed);
		}
		
		if(keyCode == KeyEvent.VK_S){
			key_s.toogle(isPressed);
		}
		
		if(keyCode == KeyEvent.VK_A){
			key_a.toogle(isPressed);
		}
		
		if(keyCode == KeyEvent.VK_D){
			key_d.toogle(isPressed);
		}
		
		if(keyCode == KeyEvent.VK_ENTER){
			enter.toogle(isPressed);
		}
		
		if(keyCode == KeyEvent.VK_SPACE){
			space.toogle(isPressed);
		}
		
		if(keyCode == KeyEvent.VK_ESCAPE){
			esc.toogle(isPressed);
		}
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		this.updateMousePosition(e);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		this.updateMousePosition(e);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		this.updateMousePosition(e);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		this.updateMousePosition(e);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.updateMousePosition(e);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		this.updateMousePosition(e);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		this.updateMousePosition(e);
	}

	private void updateMousePosition(MouseEvent e){
		this.mousePosition = new Vector2f(e.getPoint().x, e.getPoint().y);
	}
	
}
